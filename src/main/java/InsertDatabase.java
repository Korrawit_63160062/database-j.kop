
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Acer
 */
public class InsertDatabase {

    public static void main(String[] args) {
        // Connect database
        Connection con = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            con = DriverManager.getConnection(url);
            System.out.println("Insert data to database suscessfully.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        // Insert
        String sql = "INSERT INTO product(product_id, product_name, product_price, product_size, product_sweet_level, product_type, category_id) VALUES (?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, 5);
            stmt.setString(2, "Black coffee");
            stmt.setDouble(3, 55);
            stmt.setString(4, "SML");
            stmt.setString(5, "0123");
            stmt.setString(6, "HC");
            stmt.setInt(7, 1);
            int status = stmt.executeUpdate();
//            ResultSet key  = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println(""+ key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        // Close database
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

}
