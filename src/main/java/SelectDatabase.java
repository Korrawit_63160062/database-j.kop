
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Acer
 */
public class SelectDatabase {

    public static void main(String[] args) {
        // Connect database
        Connection con = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            con = DriverManager.getConnection(url);
            System.out.println("Select database suscessfully.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        // Selection
        String sql = "SELECT * FROM category";
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                System.out.println(rs.getInt("category_id") + " " + rs.getString("category_name"));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        // Close database
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

}
