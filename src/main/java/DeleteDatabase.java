
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Acer
 */
public class DeleteDatabase {

    public static void main(String[] args) {
        // Connect database
        Connection con = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            con = DriverManager.getConnection(url);
            System.out.println("Delete database suscessfully.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        // Delete
        String sql = "DELETE FROM product WHERE product_id=?";
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, 5);
            int status = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        // Close database
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

}
